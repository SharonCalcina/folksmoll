import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fancy_dialog/FancyAnimation.dart';
import 'package:fancy_dialog/FancyTheme.dart';
import 'package:fancy_dialog/fancy_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/CrearProd.dart';
import 'package:flutter_application_1/Pedidos.dart';
import 'package:flutter_application_1/Products.dart';
import 'package:flutter_application_1/firebase/estados.dart';
import 'package:provider/provider.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  int selected = 2;
  int currentIndex = 3;
  bool ini = true;
  bool create = false;
  bool showPassword = true;
  bool checkBox = false;
  bool button = true;
  final _formIni = GlobalKey<FormState>();
  final _formCreate = GlobalKey<FormState>();
  TextEditingController mail = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController pais = TextEditingController();
  TextEditingController correo = TextEditingController();
  TextEditingController contrasena = TextEditingController();
  TextEditingController nombres = TextEditingController();
  TextEditingController apellidos = TextEditingController();
  TextEditingController empresa = TextEditingController();

  setSelected(int val) {
    setState(() {
      selected = val;
    });
  }

  Future<void> _enviar() async {
    FirebaseFirestore.instance.collection('Usuarios').add({
      'id': correo.text.toString(),
      'pais': pais.value.text,
      'correo': correo.value.text,
      'nombres': nombres.value.text,
      'apellidos': apellidos.value.text,
      'empresa': empresa.value.text
    }).then((value) => print('Error al registrar al Usuario'));
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserRepository>(context, listen: true);
    user.emailVerified = false;
    user.passVerified = false;

    return ChangeNotifierProvider(
        create: (context) => UserRepository.instance(),
        child: (user.status == Status.Authenticated)
            ? Scaffold(
                body: SingleChildScrollView(
                    child: Container(
                  margin: EdgeInsets.only(top: 80),
                  child: Column(
                    children: [
                      Icon(
                        Icons.person,
                        size: 80,
                        color: Colors.black.withOpacity(0.8),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(user.user.email),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(
                            "0",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                          Text(
                            "1",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                          Text(
                            "3",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(
                            "Mis Favoritos",
                            style: TextStyle(fontSize: 12),
                          ),
                          Text(
                            "Carrito de Compras",
                            style: TextStyle(fontSize: 12),
                          ),
                          Text(
                            "Hostorial de Busqueda",
                            style: TextStyle(fontSize: 12),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      userContent(Icons.article_outlined,
                          "Administrar mis ordenes", Products()),
                      userContent(Icons.request_quote_outlined,
                          "Solicitud de presupuesto", Products()),
                      userContent(Icons.mail_outline, "Consultas", Products()),
                      userContent(Icons.question_answer_outlined,
                          "Administrar consultas de presupuestos", Products()),
                      userContent(Icons.collections_bookmark_outlined,
                          "Mis cupones", Products()),
                      userContent(Icons.book_online_outlined, "Mis Catalogos",
                          Products()),
                      userContent(
                          Icons.settings_outlined, "Configuracion", Products()),
                      userContent(Icons.point_of_sale_outlined,
                          "Comenzar a vender", CrearProd()),
                      GestureDetector(
                          onTap: () {
                            user.signOut();
                            mail.clear();
                            password.clear();

                            //Navigator.of(context).pop();
                          },
                          child: Container(
                            color: Colors.white,
                            padding: EdgeInsets.all(15),
                            child: Row(
                              children: [
                                SizedBox(
                                  width: 10,
                                ),
                                Text("Salir")
                              ],
                            ),
                          ))
                    ],
                  ),
                )),
                bottomNavigationBar: BottomNavigationBar(
                  onTap: (val) {
                    setState(() {
                      currentIndex = val;
                    });
                    if (currentIndex == 0) {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context) {
                        return Products();
                      }));
                    }
                    if (currentIndex == 1) {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context) {
                        return CrearProd();
                      }));
                    }
                    if (currentIndex == 2) {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context) {
                        return Pedidos();
                      }));
                    }
                  },
                  currentIndex: currentIndex,
                  iconSize: 30,
                  showUnselectedLabels: true,
                  selectedIconTheme: IconThemeData(color: Colors.cyan.shade200),
                  unselectedIconTheme:
                      IconThemeData(color: Colors.white.withOpacity(0.8)),
                  backgroundColor: Colors.black,
                  unselectedItemColor: Colors.white.withOpacity(0.8),
                  selectedItemColor: Colors.cyan.shade200,
                  items: [
                    BottomNavigationBarItem(
                        backgroundColor: Colors.black,
                        icon: Image.asset(
                          "assets/images/ICO.png",
                          width: 35,
                          height: 30,
                        ),
                        label: "Inicio"),
                    BottomNavigationBarItem(
                        backgroundColor: Colors.black,
                        icon: Icon(
                          Icons.mail_rounded,
                        ),
                        label: "Mensajes"),
                    BottomNavigationBarItem(
                        backgroundColor: Colors.black,
                        icon: Icon(
                          Icons.shopping_cart,
                        ),
                        label: "Carrito"),
                    BottomNavigationBarItem(
                        backgroundColor: Colors.black,
                        icon: Icon(
                          Icons.account_circle,
                        ),
                        label: "Mi Cuenta"),
                  ],
                ),
              )
            : Scaffold(
                backgroundColor: Colors.black.withOpacity(0.9),
                appBar: AppBar(
                  automaticallyImplyLeading: false,
                  backgroundColor: Colors.black.withOpacity(0.9),
                ),
                body: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Text(
                        "Bienvenido",
                        style: TextStyle(
                          color: Colors.white, fontSize: 16,
                          // fontWeight: FontWeight.bold
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        margin: EdgeInsets.all(25),
                        //width: MediaQuery.of(context).size.width/1.5,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.pink,
                        ),
                        child: Column(
                          children: <Widget>[
                            Row(children: <Widget>[
                              Radio(
                                activeColor: Colors.white,
                                value: 1,
                                groupValue: selected,
                                onChanged: (val) {
                                  ini = false;
                                  create = true;
                                  setSelected(val);
                                },
                              ),
                              Text(
                                "Crear cuenta",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14),
                              ),
                            ]),
                            Form(
                                key: _formCreate,
                                child: Visibility(
                                    visible: create,
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                            padding: EdgeInsets.only(
                                                left: 10, right: 10),
                                            margin: EdgeInsets.all(5),
                                            decoration: BoxDecoration(
                                                color: Colors.white
                                                    .withOpacity(0.4),
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            child: TextFormField(
                                              validator: (m) {
                                                return (m.isEmpty)
                                                    ? 'Error!. Intenta de nuevo'
                                                    : null;
                                              },
                                              decoration: InputDecoration(
                                                  prefixIcon: Icon(
                                                    Icons.location_on_rounded,
                                                    color: Colors.black
                                                        .withOpacity(0.45),
                                                  ),
                                                  errorStyle: TextStyle(
                                                      fontSize: 12,
                                                      fontStyle:
                                                          FontStyle.italic,
                                                      color:
                                                          Colors.red.shade900),
                                                  hintText: "pais",
                                                  hintStyle: TextStyle(
                                                      color:
                                                          Colors.grey.shade600,
                                                      fontSize: 14),
                                                  border: InputBorder.none),
                                              autofocus: false,
                                              controller: pais,
                                              cursorColor: Colors.white,
                                              autocorrect: true,
                                              style: TextStyle(
                                                color: Colors.black
                                                    .withOpacity(0.8),
                                                fontSize: 14,
                                              ),
                                            )),
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: 5, right: 5, bottom: 5),
                                          padding: EdgeInsets.only(
                                              left: 5, right: 10),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.4),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: TextFormField(
                                            validator: (p) {
                                              return (p.isEmpty)
                                                  ? 'Error!.Intenta de nuevo'
                                                  : null;
                                            },
                                            style: TextStyle(
                                                color: Colors.black
                                                    .withOpacity(0.8),
                                                fontSize: 14),
                                            autofocus: false,
                                            controller: correo,
                                            decoration: InputDecoration(
                                                prefixIcon: Icon(Icons.mail),
                                                errorStyle: TextStyle(
                                                    color: Colors.red.shade900,
                                                    fontSize: 12,
                                                    fontStyle:
                                                        FontStyle.italic),
                                                border: InputBorder.none,
                                                hintText: "correo electrónico",
                                                hintStyle: TextStyle(
                                                    color: Colors.grey.shade600,
                                                    fontSize: 14)),
                                            cursorColor: Colors.white,
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: 5, right: 5, bottom: 5),
                                          padding: EdgeInsets.only(
                                              left: 5, right: 10),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.4),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: TextFormField(
                                            validator: (p) {
                                              return (p.isEmpty)
                                                  ? 'Error!.Intenta de nuevo'
                                                  : null;
                                            },
                                            style: TextStyle(
                                                color: Colors.black
                                                    .withOpacity(0.8),
                                                fontSize: 14),
                                            autofocus: false,
                                            controller: contrasena,
                                            decoration: InputDecoration(
                                                fillColor: Colors.white,
                                                prefixIcon:
                                                    Icon(Icons.lock_rounded),
                                                errorStyle: TextStyle(
                                                    color: Colors.red.shade900,
                                                    fontSize: 12,
                                                    fontStyle:
                                                        FontStyle.italic),
                                                border: InputBorder.none,
                                                hintText: "Contraseña",
                                                hintStyle: TextStyle(
                                                    color: Colors.grey.shade600,
                                                    fontSize: 14)),
                                            cursorColor: Colors.white,
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: 5, right: 5, bottom: 5),
                                          padding: EdgeInsets.only(
                                              left: 5, right: 10),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.4),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: TextFormField(
                                            validator: (p) {
                                              return (p.isEmpty)
                                                  ? 'Error!.Intenta de nuevo'
                                                  : null;
                                            },
                                            style: TextStyle(
                                                color: Colors.black
                                                    .withOpacity(0.8),
                                                fontSize: 14),
                                            autofocus: false,
                                            controller: nombres,
                                            decoration: InputDecoration(
                                                prefixIcon: Icon(
                                                  Icons.person,
                                                  color: Colors.black
                                                      .withOpacity(0.45),
                                                ),
                                                errorStyle: TextStyle(
                                                    color: Colors.red.shade900,
                                                    fontSize: 12,
                                                    fontStyle:
                                                        FontStyle.italic),
                                                border: InputBorder.none,
                                                hintText: "Nombres",
                                                hintStyle: TextStyle(
                                                    color: Colors.grey.shade600,
                                                    fontSize: 14)),
                                            cursorColor: Colors.white,
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: 5, right: 5, bottom: 5),
                                          padding: EdgeInsets.only(
                                              left: 5, right: 10),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.4),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: TextFormField(
                                            validator: (p) {
                                              return (p.isEmpty)
                                                  ? 'Error!.Intenta de nuevo'
                                                  : null;
                                            },
                                            style: TextStyle(
                                                color: Colors.black
                                                    .withOpacity(0.8),
                                                fontSize: 14),
                                            autofocus: false,
                                            controller: apellidos,
                                            decoration: InputDecoration(
                                                errorStyle: TextStyle(
                                                    color: Colors.red.shade900,
                                                    fontSize: 12,
                                                    fontStyle:
                                                        FontStyle.italic),
                                                border: InputBorder.none,
                                                hintText: "Apellidos",
                                                hintStyle: TextStyle(
                                                    color: Colors.grey.shade600,
                                                    fontSize: 14)),
                                            cursorColor: Colors.white,
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: 5, right: 5, bottom: 5),
                                          padding: EdgeInsets.only(
                                              left: 5, right: 10),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.4),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: TextFormField(
                                            validator: (p) {
                                              return (p.isEmpty)
                                                  ? 'Error!.Intenta de nuevo'
                                                  : null;
                                            },
                                            style: TextStyle(
                                                color: Colors.black
                                                    .withOpacity(0.8),
                                                fontSize: 14),
                                            autofocus: false,
                                            controller: empresa,
                                            decoration: InputDecoration(
                                                prefixIcon: Icon(
                                                  Icons.location_city_rounded,
                                                  color: Colors.black
                                                      .withOpacity(0.45),
                                                ),
                                                errorStyle: TextStyle(
                                                    color: Colors.red.shade900,
                                                    fontSize: 12,
                                                    fontStyle:
                                                        FontStyle.italic),
                                                border: InputBorder.none,
                                                hintText:
                                                    "Nombre de la Pyme o empresa",
                                                hintStyle: TextStyle(
                                                    color: Colors.grey.shade600,
                                                    fontSize: 14)),
                                            cursorColor: Colors.white,
                                          ),
                                        ),
                                        RaisedButton(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20)),
                                            splashColor:
                                                Colors.white.withOpacity(0.8),
                                            textColor: Colors.black,
                                            elevation: 5,
                                            child: Text(
                                              "Registrar",
                                              style: TextStyle(fontSize: 14),
                                            ),
                                            color: Colors.cyan.shade200,
                                            onPressed: () async {
                                              if (_formCreate.currentState
                                                  .validate()) {
                                                if (await user.register(
                                                    correo.text,
                                                    contrasena.text,
                                                    nombres.text)) {
                                                  if (!user.emailVerified &&
                                                      correo.text
                                                          .toString()
                                                          .isValidEmail() &&
                                                      !user.passVerified) {
                                                    _enviar();
                                                    showDialog(
                                                        context: context,
                                                        builder: (BuildContext
                                                                context) =>
                                                            FancyDialog(
                                                                title:
                                                                    "Confirme su Registro",
                                                                descreption:
                                                                    "Verifique su cuenta en la bandeja de su correo",
                                                                animationType:
                                                                    FancyAnimation
                                                                        .BOTTOM_TOP,
                                                                theme:
                                                                    FancyTheme
                                                                        .FANCY,
                                                                gifPath:
                                                                    './assets/images/LG.png',
                                                                okFun:
                                                                    () => {
                                                                          button =
                                                                              false,
                                                                          Navigator.of(context)
                                                                              .pop()
                                                                        }));
                                                  }
                                                }
                                              }
                                              if (_formCreate.currentState
                                                  .validate()) {
                                                if (!correo.text
                                                    .toString()
                                                    .isValidEmail()) {
                                                  showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                              context) =>
                                                          AlertDialog(
                                                              backgroundColor:
                                                                  Colors.grey
                                                                      .shade400,
                                                              title: Text(
                                                                "Error",
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .red
                                                                        .shade800,
                                                                    fontSize:
                                                                        16),
                                                              ),
                                                              content: Text(
                                                                "El correo esta mal formateado",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        14),
                                                              )));
                                                } else {
                                                  if (!await user.register(
                                                      correo.text,
                                                      contrasena.text,
                                                      nombres.text)) {
                                                    if (user.emailVerified &&
                                                        user.passVerified) {
                                                      showDialog(
                                                          context: context,
                                                          builder: (BuildContext
                                                                  context) =>
                                                              AlertDialog(
                                                                  backgroundColor:
                                                                      Colors
                                                                          .grey
                                                                          .shade400,
                                                                  title: Text(
                                                                    "Error",
                                                                    textAlign:
                                                                        TextAlign
                                                                            .center,
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .red
                                                                            .shade800,
                                                                        fontSize:
                                                                            16),
                                                                  ),
                                                                  content: Text(
                                                                      "El correo ya ha sido usado por otra cuenta y la contraseña debe tener por lo menos 6 caracteres. Intente de nuevo",
                                                                      textAlign:
                                                                          TextAlign
                                                                              .justify,
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              14))));
                                                    } else if (user
                                                        .emailVerified) {
                                                      return showDialog(
                                                          context: context,
                                                          builder: (BuildContext
                                                                  context) =>
                                                              AlertDialog(
                                                                  backgroundColor:
                                                                      Colors
                                                                          .grey
                                                                          .shade400,
                                                                  title: Text(
                                                                    "Error",
                                                                    textAlign:
                                                                        TextAlign
                                                                            .center,
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .red
                                                                            .shade800,
                                                                        fontSize:
                                                                            16),
                                                                  ),
                                                                  content: Text(
                                                                      "El correo ya ha sido usado por otra cuenta. Intente de con otro correo",
                                                                      textAlign:
                                                                          TextAlign
                                                                              .justify,
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              14))));
                                                    } else if (user
                                                        .passVerified) {
                                                      return showDialog(
                                                          context: context,
                                                          builder: (BuildContext
                                                                  context) =>
                                                              AlertDialog(
                                                                  backgroundColor:
                                                                      Colors
                                                                          .grey
                                                                          .shade400,
                                                                  title: Text(
                                                                    "Error",
                                                                    textAlign:
                                                                        TextAlign
                                                                            .center,
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .red
                                                                            .shade800,
                                                                        fontSize:
                                                                            16),
                                                                  ),
                                                                  content: Text(
                                                                      "La contraseña debe tenr como minmo 6 caracteres. Intente de nuevo",
                                                                      textAlign:
                                                                          TextAlign
                                                                              .justify,
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              14))));
                                                    }
                                                  }
                                                }
                                              }
                                            })
                                      ],
                                    ))),
                            Row(children: <Widget>[
                              Radio(
                                activeColor: Colors.white,
                                value: 2,
                                groupValue: selected,
                                onChanged: (val) {
                                  setSelected(val);
                                  ini = true;
                                  create = false;
                                },
                              ),
                              Text(
                                "Iniciar sesion",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                            ]),
                            Form(
                                key: _formIni,
                                child: Visibility(
                                    visible: ini,
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                            padding: EdgeInsets.only(
                                                left: 10, right: 10),
                                            margin: EdgeInsets.all(5),
                                            decoration: BoxDecoration(
                                                color: Colors.white
                                                    .withOpacity(0.4),
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            child: TextFormField(
                                              validator: (m) {
                                                return (m.isEmpty)
                                                    ? 'Error!.Intenta de nuevo'
                                                    : null;
                                              },
                                              decoration: InputDecoration(
                                                  errorStyle: TextStyle(
                                                      fontSize: 12,
                                                      fontStyle:
                                                          FontStyle.italic,
                                                      color:
                                                          Colors.red.shade900),
                                                  hintText: "Ingresa tu correo",
                                                  hintStyle: TextStyle(
                                                      color:
                                                          Colors.grey.shade600,
                                                      fontSize: 14),
                                                  border: InputBorder.none),
                                              autofocus: false,
                                              controller: mail,
                                              cursorColor: Colors.white,
                                              autocorrect: true,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                              ),
                                            )),
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: 5, right: 5, bottom: 5),
                                          padding: EdgeInsets.only(
                                              left: 5, right: 10),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.4),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: TextFormField(
                                            obscureText: showPassword,
                                            validator: (p) {
                                              return (p.isEmpty)
                                                  ? 'Error!.Intenta de nuevo'
                                                  : null;
                                              //return 'prosigue';
                                            },
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14),
                                            autofocus: false,
                                            controller: password,
                                            decoration: InputDecoration(
                                                errorStyle: TextStyle(
                                                    color: Colors.red.shade900,
                                                    fontSize: 12,
                                                    fontStyle:
                                                        FontStyle.italic),
                                                border: InputBorder.none,
                                                hintText:
                                                    "Ingresa tu contraseña",
                                                hintStyle: TextStyle(
                                                    color: Colors.grey.shade600,
                                                    fontSize: 14)),
                                            cursorColor: Colors.white,
                                          ),
                                        ),
                                        Container(
                                          child: Row(
                                            children: <Widget>[
                                              Checkbox(
                                                  activeColor: Colors.white,
                                                  checkColor: Colors.pink,
                                                  value: checkBox,
                                                  onChanged: (val) {
                                                    setState(() {
                                                      showPassword =
                                                          !showPassword;
                                                      checkBox = val;
                                                    });
                                                  }),
                                              Text(
                                                "Mostrar contraseña",
                                                style: TextStyle(fontSize: 12),
                                              )
                                            ],
                                          ),
                                        ),
                                        RaisedButton(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          splashColor:
                                              Colors.white.withOpacity(0.8),
                                          onPressed: () async {
                                            if (_formIni.currentState
                                                .validate()) {
                                              if (await user.signIn(
                                                  mail.value.text,
                                                  password.value.text)) {
                                                /*Navigator.of(context).push(
                                                    MaterialPageRoute(builder:
                                                        (BuildContext context) {
                                                  return Products();
                                                }));*/
                                                print("donde");
                                              } else {
                                                showDialog(
                                                    context: context,
                                                    builder: (BuildContext
                                                            context) =>
                                                        AlertDialog(
                                                            backgroundColor:
                                                                Colors.grey
                                                                    .shade400,
                                                            title: Text(
                                                              "Error",
                                                              textAlign:
                                                                  TextAlign
                                                                      .center,
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .red
                                                                      .shade800,
                                                                  fontSize: 16),
                                                            ),
                                                            content: Text(
                                                                "El correo o la contraseña es incorrecto o aun no esta registrado. Intente de nuevo o registrese",
                                                                textAlign:
                                                                    TextAlign
                                                                        .justify,
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        14))));
                                              }
                                            }
                                          },
                                          textColor: Colors.black,
                                          elevation: 5,
                                          child: Text(
                                            "Continuar",
                                            style: TextStyle(fontSize: 14),
                                          ),
                                          color: Colors.cyan.shade200,
                                        )
                                      ],
                                    )))
                          ],
                        ),
                      ),
                      /*Container(height: 100,
            color: Colors.red,),
            Container(height: 500,
            color: Colors.white,),
            Container(height: 100,
            color: Colors.pink,)*/
                    ],
                  ),
                ),
                bottomNavigationBar: BottomNavigationBar(
                  onTap: (val) {
                    setState(() {
                      currentIndex = val;
                    });
                    if (currentIndex == 0) {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context) {
                        return Products();
                      }));
                    }
                    if (currentIndex == 1) {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context) {
                        return CrearProd();
                      }));
                    }
                    if (currentIndex == 2) {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context) {
                        return Pedidos();
                      }));
                    }
                  },
                  currentIndex: currentIndex,
                  iconSize: 30,
                  showUnselectedLabels: true,
                  selectedIconTheme: IconThemeData(color: Colors.cyan.shade200),
                  unselectedIconTheme:
                      IconThemeData(color: Colors.white.withOpacity(0.8)),
                  backgroundColor: Colors.black,
                  unselectedItemColor: Colors.white.withOpacity(0.8),
                  selectedItemColor: Colors.cyan.shade200,
                  items: [
                    BottomNavigationBarItem(
                        backgroundColor: Colors.black,
                        icon: Image.asset(
                          "assets/images/ICO.png",
                          width: 35,
                          height: 30,
                        ),
                        label: "Inicio"),
                    BottomNavigationBarItem(
                        backgroundColor: Colors.black,
                        icon: Icon(
                          Icons.mail_rounded,
                        ),
                        label: "Mensajes"),
                    BottomNavigationBarItem(
                        backgroundColor: Colors.black,
                        icon: Icon(
                          Icons.shopping_cart,
                        ),
                        label: "Carrito"),
                    BottomNavigationBarItem(
                        backgroundColor: Colors.black,
                        icon: Icon(
                          Icons.account_circle,
                        ),
                        label: "Mi Cuenta"),
                  ],
                ),
              ));
  }

  Widget userContent(icono, String titulo, pagetogo) {
    return GestureDetector(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return pagetogo;
          }));
        },
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.all(15),
          child: Row(
            children: [
              Icon(icono),
              SizedBox(
                width: 20,
              ),
              Text(titulo)
            ],
          ),
        ));
  }

  Widget msge() {
    return Text(
      'Error!. Intenta de nuevo',
      style: TextStyle(fontSize: 12),
    );
  }
}

extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_application_1/Usuarios.dart';
import 'dart:async';

final CollectionReference prodColl =
    FirebaseFirestore.instance.collection('Usuarios');

class FirebaseUsuarios {
  static final FirebaseUsuarios _instance = new FirebaseUsuarios.internal();
  factory FirebaseUsuarios() => _instance;
  FirebaseUsuarios.internal();
  Future<Usuarios> createProd(
    String id,
    String pais,
    String correo,
    String nombres,
    String apellidos,
    String empresa,
  ) {
    final TransactionHandler createTransaction = (Transaction tx) async {
      final DocumentSnapshot doc = await tx.get(prodColl.doc());
      final Usuarios prod = new Usuarios(
        doc.id,
        pais,
        correo,
        nombres,
        apellidos,
        empresa,
      );
      final Map<String, dynamic> data = prod.toMap();
      tx.set(doc.reference, data);
      return data;
    };
    return FirebaseFirestore.instance
        .runTransaction(createTransaction)
        .then((mapData) {
      return Usuarios.fromMap(mapData);
    }).catchError((e) {
      print('e:$e');
      return null;
    });
  }

  Stream<QuerySnapshot> getListProd({int offset, int limit}) {
    Stream<QuerySnapshot> snapShot = prodColl.snapshots();
    if (offset != null) {
      snapShot = snapShot.skip(offset);
    }
    if (limit != null) {
      snapShot = snapShot.skip(limit);
    }
    return snapShot;
  }
}

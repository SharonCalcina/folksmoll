import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';
import 'package:flutter_application_1/prodModel.dart';

final CollectionReference prodColl =
    FirebaseFirestore.instance.collection('BellezaYCuidadoPersonal');

class FirebaseBellezaCPersonal {
  static final FirebaseBellezaCPersonal _instance =
      new FirebaseBellezaCPersonal.internal();
  factory FirebaseBellezaCPersonal() => _instance;
  FirebaseBellezaCPersonal.internal();
  Future<ProdModel> createProd(
      String imagen,
      String descripcion,
      String precio,
      String modelo,
      String marca,
      String origen,
      String otrosDetalles,
      String tiempoDelivery,
      String detallesEnvio,
      String dirEmpresa,
      String nombreContacto,
      String nombreCompania,
      String nroCel,
      String tipoNegocio,
      String prodPrincipal,
      String terminoPago,
      String capacidadProduccion,
      String cantidadProductos) {
    final TransactionHandler createTransaction = (Transaction tx) async {
      final DocumentSnapshot doc = await tx.get(prodColl.doc());
      final ProdModel prod = new ProdModel(
          doc.id,
          imagen,
          descripcion,
          precio,
          modelo,
          marca,
          origen,
          otrosDetalles,
          tiempoDelivery,
          detallesEnvio,
          dirEmpresa,
          nombreContacto,
          nombreCompania,
          nroCel,
          tipoNegocio,
          prodPrincipal,
          terminoPago,
          capacidadProduccion,
          cantidadProductos);
      final Map<String, dynamic> data = prod.toMap();
      tx.set(doc.reference, data);
      return data;
    };
    return FirebaseFirestore.instance
        .runTransaction(createTransaction)
        .then((mapData) {
      return ProdModel.fromMap(mapData);
    }).catchError((e) {
      print('e:$e');
      return null;
    });
  }

  Stream<QuerySnapshot> getListProd({int offset, int limit}) {
    Stream<QuerySnapshot> snapShot = prodColl.snapshots();
    if (offset != null) {
      snapShot = snapShot.skip(offset);
    }
    if (limit != null) {
      snapShot = snapShot.skip(limit);
    }
    return snapShot;
  }
}

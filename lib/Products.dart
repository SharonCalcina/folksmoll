import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/Paginas/ArtBebes.dart';
import 'package:flutter_application_1/Paginas/ArtDeportivo.dart';
import 'package:flutter_application_1/Paginas/ArtElectronicos.dart';
import 'package:flutter_application_1/Paginas/ArtMascotas.dart';
import 'package:flutter_application_1/Paginas/Artesanias.dart';
import 'package:flutter_application_1/Paginas/BellezaCPersonal.dart';
import 'package:flutter_application_1/Paginas/ConfRep.dart';
import 'package:flutter_application_1/Paginas/Construccion.dart';
import 'package:flutter_application_1/Paginas/HogarCocina.dart';
import 'package:flutter_application_1/Paginas/JoyeriaAccesorios.dart';
import 'package:flutter_application_1/Paginas/Jugueteria.dart';
import 'package:flutter_application_1/Paginas/Otros.dart';
import 'package:flutter_application_1/Paginas/RopaZapatos.dart';
import 'package:flutter_application_1/db.dart';
import 'package:flutter_application_1/firebase/estados.dart';
import 'package:provider/provider.dart';

import 'Pedidos.dart';
import 'Register.dart';

class Products extends StatefulWidget {
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  String _p1 = "Artesanias";
  String _p2 = "Hogar y Cocina";
  String _p3 = "Belleza y Cuidado Personal";
  String _p4 = "Jugueteria";
  String _p5 = "Joyeria y Accesorios";
  String _p6 = "Articulos Electronicos";
  String _p7 = "Articulos Deportivos";
  String _p8 = "Ropa y Zapatos";
  String _p9 = "Articulos para bebes";
  String _p10 = "Articulos para Mascotas";
  String _p11 = "Confiteria y Reposteria";
  String _p12 = "Construccion";
  String _p13 = "Otros";
  int currentIndex = 0;

  List items = [
    "assets/images/b.jpg",
    "assets/images/b.jpg",
    "assets/images/hp.jpg",
    "assets/images/b.jpg",
    "assets/images/b.jpg",
    "assets/images/b.jpg",
    "assets/images/hp.jpg",
    "assets/images/b.jpg"
  ];

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    final user = Provider.of<UserRepository>(context, listen: true);
    //double h = MediaQuery.of(context).size.height;
    return ChangeNotifierProvider(
        create: (context) => UserRepository.instance(),
        child: DefaultTabController(
            length: 13,
            child: Scaffold(
              appBar: AppBar(
                automaticallyImplyLeading: false,
                title: Image.asset('assets/images/LG.png', width: 110),
                actions: <Widget>[
                  GestureDetector(
                      onTap: () {},
                      child: Icon(
                        Icons.qr_code_scanner,
                        size: 35,
                        color: Colors.white.withOpacity(0.8),
                      )),
                  GestureDetector(
                      onTap: () {
                        showSearch(context: context, delegate: SearchDele());
                      },
                      child: Container(
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.only(top: 5, bottom: 5, left: 10),
                        width: w - 200,
                        height: 10,
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Icon(
                              Icons.search,
                              color: Colors.white,
                            )),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.black.withOpacity(0.2),
                        ),
                      )),
                  //),
                ],
                backgroundColor: Colors.pink,
                bottom: PreferredSize(
                  preferredSize: Size.fromHeight(45),
                  child: TabBar(
                    isScrollable: true,
                    indicatorColor: Colors.cyan,
                    labelColor: Colors.white,
                    indicatorSize: TabBarIndicatorSize.label,
                    tabs: <Widget>[
                      Tab(
                        child: Text(_p1),
                      ),
                      Tab(
                        child: Text(_p2),
                      ),
                      Tab(
                        child: Text(_p3),
                      ),
                      Tab(
                        child: Text(_p4),
                      ),
                      Tab(
                        child: Text(_p5),
                      ),
                      Tab(
                        child: Text(_p6),
                      ),
                      Tab(
                        child: Text(_p7),
                      ),
                      Tab(
                        child: Text(_p8),
                      ),
                      Tab(
                        child: Text(_p9),
                      ),
                      Tab(
                        child: Text(_p10),
                      ),
                      Tab(
                        child: Text(_p11),
                      ),
                      Tab(
                        child: Text(_p12),
                      ),
                      Tab(
                        child: Text(_p13),
                      )
                    ],
                  ),
                ),
              ),

              /*  String _p1 = "Artesanias";
  String _p2 = "Hogar y Cocina";
  String _p3 = "Belleza y Cuidado Personal";
  String _p4 = "Jugueteria";
  String _p5 = "Joyeria y Accesorios";
  String _p6 = "Articulos Electronicos";
  String _p7 = "Articulos Deportivos";
  String _p8 = "Ropa y Zapatos";
  String _p9 = "Articulos para bebes";
  String _p10 = "Articulos para Mascotas";
  String _p11 = "Confiteria y Reposteria";
  String _p12 = "Construccion";
  String _p13 = "Otros"; */
              body: TabBarView(
                children: [
                  Artesanias(),
                  HogarCocina(),
                  BellezaCPersonal(),
                  Jugueteria(),
                  JoyeriaAccesorios(),
                  ArtElectronicos(),
                  ArtDeportivo(),
                  RopaZapatos(),
                  ArtBebes(),
                  ArtMascotas(),
                  ConfRep(),
                  Construccion(),
                  Otros()
                ],
              ),
              bottomNavigationBar: BottomNavigationBar(
                onTap: (val) {
                  setState(() {
                    currentIndex = val;
                  });
                  if (currentIndex == 3) {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) {
                      return Register();
                    }));
                  }
                  if (currentIndex == 1) {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) {
                      return Cart(user.listaCar);
                    }));
                  }
                  if (currentIndex == 2) {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) {
                      return Pedidos();
                    }));
                  }
                },
                currentIndex: currentIndex,
                iconSize: 30,
                showUnselectedLabels: true,
                selectedIconTheme: IconThemeData(color: Colors.cyan.shade200),
                unselectedIconTheme:
                    IconThemeData(color: Colors.white.withOpacity(0.8)),
                backgroundColor: Colors.black,
                unselectedItemColor: Colors.white.withOpacity(0.8),
                selectedItemColor: Colors.cyan.shade200,
                items: [
                  BottomNavigationBarItem(
                      backgroundColor: Colors.black,
                      icon: Image.asset(
                        "assets/images/ICO.png",
                        width: 35,
                        height: 30,
                      ),
                      label: "Inicio"),
                  BottomNavigationBarItem(
                      backgroundColor: Colors.black,
                      icon: Icon(
                        Icons.mail_rounded,
                      ),
                      label: "Mensajes"),
                  BottomNavigationBarItem(
                      backgroundColor: Colors.black,
                      icon: Icon(
                        Icons.shopping_cart,
                      ),
                      label: "Carrito"),
                  BottomNavigationBarItem(
                      backgroundColor: Colors.black,
                      icon: Icon(
                        Icons.account_circle,
                      ),
                      label: "Mi Cuenta"),
                ],
              ),
            )));
  }

  Widget text(name) {
    return Text(
      name,
      style: TextStyle(color: Colors.cyan.shade200, fontSize: 12),
    );
  }
}

class SearchDele extends SearchDelegate {
  List recent = ["cartera", "deportivo", "audifonos", "led", "juegos de mesa"];
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            this.query = '';
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Scaffold(
      body: Container(
          child: CustomScrollView(slivers: [
        SliverGrid(
          delegate: SliverChildBuilderDelegate((context, item) {
            return Card(
                elevation: 1,
                color: Colors.black,
                //color: Colors.grey.shade300,
                child: Column(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          //image:DecorationImage(image: ),
                          borderRadius: BorderRadius.all(Radius.circular(0.8)),
                          color: Colors.white),
                      margin: EdgeInsets.all(10),
                      height: 150,
                      width: 200,
                      child:
                          Image.asset("assets/images/b.jpg", fit: BoxFit.cover),
                    ),
                    Container(
                      padding: EdgeInsets.all(5),
                      height: 58,
                      //color: Colors.amberAccent,
                      child: Column(
                        children: <Widget>[
                          Text(
                            "description----------------------------------------..........................................................................................hhhhhhhhhhhhhhhhhhhhhhhhhh",
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.white.withOpacity(0.6)),
                            maxLines: 3,
                          ),
                        ],
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.centerLeft,
                          //width: 135,
                          //color: Colors.pink,
                          margin: EdgeInsets.only(left: 5, bottom: 3),
                          child: Text(
                            "Price in BS ".toUpperCase(),
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white.withOpacity(0.9)),
                          ),
                        ),
                        InkWell(
                          borderRadius: BorderRadius.circular(20),
                          onTap: () {},
                          child: Container(
                            //color: Colors.red,
                            width: 20,
                            alignment: Alignment.centerRight,
                            margin: EdgeInsets.only(
                                right: 8, bottom: 3, left: 8, top: 5),
                            child: Icon(
                              Icons.add_shopping_cart,
                              color: Colors.cyan.shade200,
                            ),
                          ),
                          //focusColor: Colors.amber,
                          //highlightColor: Colors.blue,
                          splashColor: Colors.white.withOpacity(0.5),
                        )
                      ],
                    ),
                    Text(
                      "UNIDADES ---------------------".toString(),
                      maxLines: 1,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey.shade600),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Text(
                          "calificacion",
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.yellow),
                        ))
                  ],
                ));
          }, childCount: 10),
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              childAspectRatio: 0.63,
              mainAxisSpacing: 5,
              maxCrossAxisExtent: 200,
              crossAxisSpacing: 5),
        ),
      ])),
    );
    /*CircularProgressIndicator(
      backgroundColor: Colors.amber,
    );*/
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return ListView.builder(
        itemCount: recent.length,
        itemBuilder: (_, item) {
          return ListTile(
            trailing: Icon(Icons.arrow_forward),
            title: Text(recent[item].toString()),
            onTap: () {
              query = recent[item];
            },
          );
        });
  }
}

/*flexibleSpace: Column(
                  children: <Widget>[
                    SizedBox(
                      height:h/9.5,
                    ),
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Icon(
                            Icons.qr_code_scanner,
                            size: 35,
                            color: Colors.white.withOpacity(0.8),
                          ),
                          /*SizedBox(
                      width: w/4,
                    ),*/
                          /*Expanded(child:Image(
                      image: AssetImage('assets/images/LG.png'),
                      height: 45,
                      width: 100,
                    )),*/
                          /*SizedBox(
                      width: w/7,
                    ),
                    Container(
                          margin: EdgeInsets.only(right: 15),
                        child:InkWell(
                        highlightColor: Colors.white.withOpacity(0.8),
                        radius: 5,
                        onTap: () {},
                        borderRadius: BorderRadius.circular(20),
                        child: Icon(
                          Icons.shopping_cart,
                          color: Colors.white,
                          size: 35,
                          
                        )))
                  ],
                ),
                SizedBox(
                  height: 10,
                ),*/
                          GestureDetector(
                              child: InkWell(
                                  highlightColor: Colors.white.withOpacity(0.8),
                                  borderRadius: BorderRadius.circular(5),
                                  onTap: () {},
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        top: 5, bottom: 5, left: 10),
                                    width: w - 80,
                                    child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Icon(
                                          Icons.search,
                                          color: Colors.white,
                                        )),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.black.withOpacity(0.2),
                                    ),
                                  )))
                        ])
                  ],
                ),*/

/*Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.pink,
            flexibleSpace: Column(children: <Widget>[
              SizedBox(
                height: 50,
              ),
              Image(
                image: AssetImage("assets/images/LG.png"),
                height: MediaQuery.of(context).size.height / 15,
                width: 200,
              )
            ]),*/

//child: InkWell(
//splashColor: Colors.white.withOpacity(0.2),
//  borderRadius: BorderRadius.circular(5),
/*SliverList(
            delegate: SliverChildListDelegate(
              [
                HeaderWidget("Header 1"),
                HeaderWidget("Header 2"),
                HeaderWidget("Header 3"),
                HeaderWidget("Header 4"),
              ],
            ),
          ),*/
/* instead of circle avantar then
          ClipOval(
  child: Image.asset(
    'assets/cat.jpg',
    fit: BoxFit.cover,
  ),
)
(iii) Use ClipRRect

ClipRRect(
  borderRadius: BorderRadius.circular(50),
  child: Image.asset(
    'assets/cat.jpg',
    fit: BoxFit.cover,
  ),
)*/
/*Text(
                        "TItle--------------------------------------------------------".toUpperCase(),
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.bold),
                      ),*/
/*Container(
              color: Colors.black,
              padding: EdgeInsets.all(5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.black,
                        child: Image.asset(
                          "assets/images/ICO.png",
                        ),
                      ),
                      text("Inicio")
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      SizedBox(
                        height: 4,
                      ),
                      GestureDetector(
                          onTap: () {},
                          child: Icon(
                            Icons.mail_rounded,
                            color: Colors.white,
                            size: 32,
                          )),
                      SizedBox(
                        height: 4,
                      ),
                      text("Mensajes")
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      SizedBox(
                        height: 10,
                      ),
                      GestureDetector(
                          onTap: () {},
                          child: Icon(
                            Icons.shopping_cart,
                            color: Colors.white,
                            size: 30,
                          )),
                      text("Carrito")
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      GestureDetector(
                          onTap: () {},
                          child: Icon(
                            Icons.account_circle,
                            color: Colors.white,
                            size: 36,
                          )),
                      SizedBox(
                        height: 2,
                      ),
                      text("Mi cuenta")
                    ],
                  )
                ],
              ),
            )*/
/*Text(
                        "TItle--------------------------------------------------------".toUpperCase(),
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.bold),
                      ),*/

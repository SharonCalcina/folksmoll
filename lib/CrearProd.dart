import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class CrearProd extends StatefulWidget {
  @override
  _CrearProdState createState() => _CrearProdState();
}

class _CrearProdState extends State<CrearProd> {
  final form1 = GlobalKey<FormState>();
  final form2 = GlobalKey<FormState>();
  bool one = true;
  bool two = false;
  bool three = false;
  TextEditingController descripcion = TextEditingController();
  TextEditingController precio = TextEditingController();
  //TextEditingController tipoProd = TextEditingController();

  TextEditingController nromodel = TextEditingController();
  TextEditingController marca = TextEditingController();
  TextEditingController origen = TextEditingController();
  TextEditingController otrosDet = TextEditingController();

  TextEditingController tiempoEntrega = TextEditingController();
  TextEditingController detalleEmpaque = TextEditingController();
  //TextEditingController capacidad = TextEditingController();
  TextEditingController ubicacion = TextEditingController();
  TextEditingController nomContant = TextEditingController();
  TextEditingController nomComp = TextEditingController();
  TextEditingController nroCel = TextEditingController();
  TextEditingController tipoNeg = TextEditingController();
  TextEditingController prodPrin = TextEditingController();

  File imageFile;
  bool _isInAsyncCall = false;
  String urlFoto;
  String imagen;
  String valorPagina;

  Future<void> _enviar() async {
    String paginaActual;
    if (valorPagina == "Artesanias") {
      paginaActual = 'Artesanias';
    } else if (valorPagina == "Hogar y Cocina") {
      paginaActual = 'HogarYCocina';
    } else if (valorPagina == "Belleza y Cuidado Personal      ") {
      paginaActual = 'BellezaYCuidadoPersonal';
    } else if (valorPagina == "Jugueteria") {
      paginaActual = 'Jugueteria';
    } else if (valorPagina == "Joyeria y Accesorios") {
      paginaActual = 'JoyeriaYAccesorios';
    } else if (valorPagina == "Articulos Electronicos") {
      paginaActual = 'ArticulosElectronicos';
    } else if (valorPagina == "Articulos Deportivos") {
      paginaActual = 'ArticulosDeportivos';
    } else if (valorPagina == "Ropa y Zapatos") {
      paginaActual = 'HogarYCocina';
    } else if (valorPagina == "Articulos para bebes") {
      paginaActual = 'BellezaYCuidadoPersonal';
    } else if (valorPagina == "Articulos para Mascotas") {
      paginaActual = 'Jugueteria';
    } else if (valorPagina == "Confiteria y Reposteria") {
      paginaActual = 'JoyeriaYAccesorios';
    } else if (valorPagina == "Construccion") {
      paginaActual = 'ArticulosElectronicos';
    } else if (valorPagina == "Otros") {
      paginaActual = 'ArticulosDeportivos';
    }
    /* SettableMetadata metadata = SettableMetadata(
      cacheControl: 'max-age=60',
      customMetadata: <String, String>{
        'imagen': 'ABC123',
      },
    );*/
    if (imageFile != null) {
      String nom = nomContant.value.text;
      final Reference fireStoreRef =
          FirebaseStorage.instance.ref().child(paginaActual).child('$nom.jpg');
      final UploadTask task = fireStoreRef.putFile(imageFile);
      await task.whenComplete(() {
        fireStoreRef.getDownloadURL().then((onValue) {
          setState(() {
            urlFoto = onValue.toString();
            print(urlFoto);
            FirebaseFirestore.instance
                .collection(paginaActual)
                .add({
                  'id': urlFoto,
                  'imagen': urlFoto,
                  'descripcion': descripcion.value.text,
                  'precio': precio.text,
                  'modelo': nromodel.value.text,
                  'marca': marca.value.text,
                  'origen': origen.value.text,
                  'otrosDetalles': otrosDet.value.text,
                  'tiempoDelivery': tiempoEntrega.value.text,
                  'detallesEnvio': descripcion.value.text,
                  'dirEmpresa': ubicacion.value.text,
                  'nombreContacto': nomContant.value.text,
                  'nombreCompañia': nomComp.value.text,
                  'nroCel': nroCel.value.text,
                  'prodPrincipal': prodPrin.value.text,
                  'terminoPago': "",
                  'capacidadProduccion': "",
                  'cantidadProductos': "",
                  'tipoNegocio': tipoNeg.value.text,
                })
                .then((value) => Navigator.of(context).pop())
                .catchError(
                    (onError) => print('Error al registrar su produtos bd'));
            _isInAsyncCall = false;
          });
        });
      });
    } else {
      FirebaseFirestore.instance
          .collection('productos')
          .add({
            'id': urlFoto,
            'imagen': urlFoto,
            'descripcion': descripcion.value.toString(),
            'precio': precio.toString(),
            'modelo': nromodel.value.toString(),
            'marca': marca.value.toString(),
            'origen': origen.value.toString(),
            'otrosDetalles': otrosDet.value.toString(),
            'tiempoDelivery': tiempoEntrega.value.toString(),
            'detallesEnvio': descripcion.value.toString(),
            'dirEmpresa': ubicacion.value.toString(),
            'nombreContacto': nomContant.value.toString(),
            'nombreCompañia': nomComp.value.toString(),
            'nroCel': nroCel.value.toString(),
            'prodPrincipal': prodPrin.value.toString(),
            'terminoPago': "",
            'capacidadProduccion': "",
            'cantidadProductos': "",
            'tipoNegocio': tipoNeg.value.toString(),
          })
          .then((value) => Navigator.of(context).pop())
          .catchError((onError) => print('Error al registrar su producto'));
      _isInAsyncCall = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        leadingWidth: 80,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                //bottomLeft: Radius.circular(20),
                bottomRight: Radius.circular(100))),
        elevation: 20,
        shadowColor: Colors.white,
        backgroundColor: Colors.pink,
        titleSpacing: 2,
        centerTitle: true,
        title: Text(
          "Venta de Productos",
          style: TextStyle(fontSize: 14),
        ),
      ),
      body: ModalProgressHUD(
          inAsyncCall: _isInAsyncCall,
          opacity: 0.5,
          dismissible: false,
          progressIndicator: CircularProgressIndicator(),
          color: Colors.blueGrey,
          child: SingleChildScrollView(
              child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 40, bottom: 10),
                  height: MediaQuery.of(context).size.width / 1.8,
                  width: MediaQuery.of(context).size.width / 1.6,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),

                    //decoration:
                    //  BoxDecoration(borderRadius: BorderRadius.circular(20)),
                    child: _setImageView(),
                  ),
                ),
                Text(
                  "Ingresa una Imagen",
                  style: TextStyle(
                      color: Colors.cyan.shade200,
                      fontStyle: FontStyle.italic,
                      fontSize: 12),
                ),
                Form(
                  key: form1,
                  child: Visibility(
                      visible: one,
                      child: Column(
                        children: [
                          textForm("Descripcion del producto", descripcion, 2),
                          textFormNumber("Precio", precio),
                          SizedBox(
                            height: 25,
                          ),
                          Text(
                            "Detalles del producto",
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                              //fontWeight: FontWeight.bold
                            ),
                          ),
                          textForm("Numero de modelo", nromodel, 1),
                          textForm("Nombre de marca", marca, 1),
                          textForm("Lugar de origen", origen, 1),
                          textFormNumber("Unidades disponibles del producto", otrosDet),
                          SizedBox(
                            height: 10,
                          ),
                          DropdownButton<String>(
                            iconSize: 50,
                            iconEnabledColor: Colors.pink,
                            underline: Container(
                              child: Text(
                                "Selecciona la categoria del producto",
                                style: TextStyle(
                                    color: Colors.cyan.shade200,
                                    fontSize: 12,
                                    fontStyle: FontStyle.italic),
                              ),
                              height: 20,
                              color: Colors.transparent,
                            ),
                            dropdownColor: Colors.grey,
                            value: valorPagina,
                            onChanged: (String newValue) {
                              print(newValue);
                              setState(() {
                                valorPagina = newValue;
                              });
                            },
                            items: <String>[
                              "Artesanias",
                              "Hogar y Cocina",
                              "Belleza y Cuidado Personal      ",
                              "Jugueteria",
                              "Joyeria y Accesorios",
                              "Articulos Electronicos",
                              "Articulos Deportivos",
                              "Ropa y Zapatos",
                              "Articulos para bebes",
                              "Articulos para Mascotas",
                              "Confiteria y Reposteria",
                              "Construccion",
                              "Otros"
                            ].map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(
                                  value,
                                  style: TextStyle(fontSize: 14),
                                ),
                              );
                            }).toList(),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          FlatButton(
                              onPressed: () {
                                setState(() {
                                  if (form1.currentState.validate()) {
                                    two = true;
                                    one = false;
                                  }
                                });
                              },
                              child: Text("Continuar"),
                              color: Colors.cyan.shade200,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20))),
                          SizedBox(
                            height: 80,
                          )
                        ],
                      )),
                ),
                Form(
                    key: form2,
                    child: Visibility(
                        visible: two,
                        child: Column(
                          children: [
                            textForm("Tiempo de delivery o entrega",
                                tiempoEntrega, 1),
                            textForm("Detalles de envío", detalleEmpaque, 1),
                            textForm(
                                "Direccion de la empresa o Pyme", ubicacion, 1),
                            textForm("Nombre de contacto", nomContant, 1),
                            textForm("Nombre de la compañía", nomComp, 1),
                            textFormNumCel("Numero de celular", nroCel),
                            textForm("Tipo de negocio", tipoNeg, 1),
                            textForm("Producto principal", prodPrin, 1),
                            SizedBox(
                              height: 10,
                            ),
                            FlatButton(
                                onPressed: () {
                                  setState(() {
                                    if (form2.currentState.validate()) {
                                      //two = false;
                                      _isInAsyncCall = true;
                                      _enviar();
                                    }
                                  });
                                },
                                child: Text("Enviar"),
                                color: Colors.cyan.shade200,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20))),
                            SizedBox(
                              height: 60,
                            )
                          ],
                        )))
              ],
            ),
          ))),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.pink,
          onPressed: () {
            _showSelectionDialog(context);
          },
          child: Icon(
            Icons.camera_alt,
            size: 32,
          )),
      /*bottomSheet: Container(
          color: Colors.transparent,
          margin: EdgeInsets.only(
              left: MediaQuery.of(context).size.width / 1.25, bottom: 5),
          height: 60,
          child: Visibility(
            visible: one,
            child: FloatingActionButton(
              onPressed: () {
                _showSelectionDialog(context);
              },
              child: Icon(Icons.camera_alt),
            ),
          ),
        )*/
      /*floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showSelectionDialog(context);
          setState(() {
            
            if(two){
            ;}
          });
        },
        child: Icon(Icons.camera_alt),
      ),*/
    );
  }

  Widget textFormNumber(data, controll) {
    return Container(
        padding: EdgeInsets.all(5),
        margin: EdgeInsets.only(left: 30, right: 30, top: 5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
        ),
        child: TextFormField(
          keyboardAppearance: Brightness.dark,
          keyboardType: TextInputType.numberWithOptions(decimal: true),
          cursorColor: Colors.white,
          autocorrect: true,
          autofocus: false,
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                // borderRadius: BorderRadius.only(bottomLeft:Radius.circular(20)),
                borderSide: BorderSide(
                  color: Colors.pink.shade800,
                  //style: BorderStyle.solid,
                  width: 3,
                ),
              ),
              enabledBorder: UnderlineInputBorder(
                // borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Colors.grey.shade700,
                  style: BorderStyle.solid,
                  width: 3,
                ),
              ),
              hintText: data,
              hintStyle: TextStyle(
                  color: Colors.white.withOpacity(0.8),
                  fontStyle: FontStyle.italic,
                  fontSize: 12)),
          style: TextStyle(
            fontSize: 14,
            color: Colors.white,
          ),
          controller: controll,
          validator: (val) {
            return (val.isEmpty) ? 'Ingrese su respuesta' : null;
          },
          maxLines: 1,
        ));
  }

  Widget textFormNumCel(data, controll) {
    return Container(
        padding: EdgeInsets.all(5),
        margin: EdgeInsets.only(left: 30, right: 30, top: 5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
        ),
        child: TextFormField(
          keyboardAppearance: Brightness.dark,
          keyboardType: TextInputType.number,
          cursorColor: Colors.white,
          autocorrect: true,
          autofocus: false,
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                // borderRadius: BorderRadius.only(bottomLeft:Radius.circular(20)),
                borderSide: BorderSide(
                  color: Colors.pink.shade800,
                  //style: BorderStyle.solid,
                  width: 3,
                ),
              ),
              enabledBorder: UnderlineInputBorder(
                // borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Colors.grey.shade700,
                  style: BorderStyle.solid,
                  width: 2,
                ),
              ),
              hintText: data,
              hintStyle: TextStyle(
                  color: Colors.white.withOpacity(0.8),
                  fontStyle: FontStyle.italic,
                  fontSize: 12)),
          style: TextStyle(fontSize: 14, color: Colors.white),
          controller: controll,
          validator: (val) {
            return (val.isEmpty) ? 'Ingrese su respuesta' : null;
          },
          maxLines: 1,
        ));
  }

  Widget textForm(data, controll, ml) {
    return Container(
        padding: EdgeInsets.all(5),
        margin: EdgeInsets.only(left: 30, right: 30, top: 5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10), color: Colors.transparent
            //color: Colors.white.withOpacity(0.65)
            ),
        child: TextFormField(
          // keyboardAppearance: Brightness.dark,
          cursorColor: Colors.white,
          autocorrect: true,
          autofocus: false,
          decoration: InputDecoration(
              focusColor: Colors.white,
              fillColor: Colors.amber,
              focusedBorder: UnderlineInputBorder(
                // borderRadius: BorderRadius.only(bottomLeft:Radius.circular(20)),
                borderSide: BorderSide(
                  color: Colors.pink.shade800,
                  //style: BorderStyle.solid,
                  width: 3,
                ),
              ),
              enabledBorder: UnderlineInputBorder(
                // borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Colors.grey.shade700,
                  style: BorderStyle.solid,
                  width: 2,
                ),
              ),
              //border: InputBorder.none,
              hintText: data,
              hintStyle: TextStyle(
                  color: Colors.white.withOpacity(0.8),
                  fontStyle: FontStyle.italic,
                  fontSize: 12)),
          style: TextStyle(
            fontSize: 14,
            color: Colors.white.withOpacity(0.8),
          ),
          controller: controll,
          validator: (val) {
            return (val.isEmpty) ? 'Ingrese su respuesta' : null;
          },
          maxLines: ml,
        ));
  }

  Future<void> _showSelectionDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              elevation: 20,
              backgroundColor: Colors.grey.shade400,
              title: Container(
                  padding: EdgeInsets.all(10),
                  height: 40,
                  decoration: BoxDecoration(
                      color: Colors.black.withOpacity(0.8),
                      borderRadius: BorderRadius.circular(10)),
                  child: Text(
                    "¿De dónde quieres tomar la foto?",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Colors.white.withOpacity(0.7)),
                  )),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    GestureDetector(
                      child: Text("Galeria",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold)),
                      onTap: () {
                        _openGallery(context);
                      },
                    ),
                    Padding(padding: EdgeInsets.all(15)),
                    GestureDetector(
                      child: Text("Camara",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold)),
                      onTap: () {
                        _openCamera(context);
                      },
                    )
                  ],
                ),
              ));
        });
  }

  void _openGallery(BuildContext context) async {
    var picture = await ImagePicker().getImage(source: ImageSource.gallery);
    this.setState(() {
      imageFile = File(picture.path);
    });
    Navigator.of(context).pop();
  }

  void _openCamera(BuildContext context) async {
    final picker = ImagePicker();
    var picture = await picker.getImage(source: ImageSource.camera);
    this.setState(() {
      imageFile = File(picture.path);
    });
    Navigator.of(context).pop();
  }

  Widget _setImageView() {
    if (imageFile != null) {
      return Image.file(
        imageFile,
        width: 500,
        height: 500,
        fit: BoxFit.fill,
      );
    } else {
      return ColorFiltered(
        child: Image.asset(
          "assets/images/LG.png",
          fit: BoxFit.fill,
          //color: Colors.white.withOpacity(0.2),
        ),
        colorFilter: ColorFilter.mode(Colors.cyan.shade100, BlendMode.color),
      );
    }
  }
}

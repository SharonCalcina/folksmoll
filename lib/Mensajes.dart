import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';

class Mensajes extends StatefulWidget {
  @override
  _MensajesState createState() => _MensajesState();
}

class _MensajesState extends State<Mensajes> {
  List items=["assets/images/b.jpg",
  "assets/images/b.jpg","assets/images/hp.jpg","assets/images/b.jpg", "assets/images/b.jpg",
  "assets/images/b.jpg","assets/images/hp.jpg","assets/images/b.jpg"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ListView"),
      ),
      body: Container(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Container(
                height: 150,
                child: Carousel(
                  //boxFit: BoxFit.cover,
                  animationDuration: Duration(microseconds: 5),
                  images: <Widget>[
                    Image.asset(items[0], fit: BoxFit.cover,),
                    Image.asset(items[6], fit: BoxFit.cover,)
                  ],

                )
                /*
                ListView(
                  dragStartBehavior: DragStartBehavior((context{

                  })),
                  scrollDirection: Axis.horizontal,
                  children: items.map((item) {
                    return Image.asset(item, width: 400,);
                  }).toList(),
                ),*/
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  BodyWidget(Colors.blue),
                  BodyWidget(Colors.red),
                  BodyWidget(Colors.green),
                  BodyWidget(Colors.orange),
                  BodyWidget(Colors.blue),
                  BodyWidget(Colors.red),
                ],
              ),
            ),
            SliverGrid(
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  mainAxisSpacing: 5,
                  crossAxisSpacing: 5,
                  maxCrossAxisExtent: 200,
                  childAspectRatio: 0.63),
              delegate: SliverChildBuilderDelegate((context, index) {
                return Card(
                    elevation: 1,
                    color: Colors.black,
                    //color: Colors.grey.shade300,
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              //image:DecorationImage(image: ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0.8)),
                              color: Colors.white),
                          margin: EdgeInsets.all(10),
                          height: 150,
                          width: 200,
                          child: Image.asset("assets/images/b.jpg",
                              fit: BoxFit.cover),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          height: 58,
                          //color: Colors.amberAccent,
                          child: Column(
                            children: <Widget>[
                              /*Text(
                        "TItle--------------------------------------------------------".toUpperCase(),
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.bold),
                      ),*/
                              Text(
                                "description----------------------------------------..........................................................................................hhhhhhhhhhhhhhhhhhhhhhhhhh",
                                textAlign: TextAlign.justify,
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.white.withOpacity(0.6)),
                                maxLines: 3,
                              ),
                            ],
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                              alignment: Alignment.centerLeft,
                              //width: 135,
                              //color: Colors.pink,
                              margin: EdgeInsets.only(left: 5, bottom: 3),
                              child: Text(
                                "Price in BS ".toUpperCase(),
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white.withOpacity(0.9)),
                              ),
                            ),
                            InkWell(
                              borderRadius: BorderRadius.circular(20),
                              onTap: () {},
                              child: Container(
                                //color: Colors.red,
                                width: 20,
                                alignment: Alignment.centerRight,
                                margin: EdgeInsets.only(
                                    right: 8, bottom: 3, left: 8, top: 5),
                                child: Icon(
                                  Icons.add_shopping_cart,
                                  color: Colors.cyan.shade200,
                                ),
                              ),
                              //focusColor: Colors.amber,
                              //highlightColor: Colors.blue,
                              splashColor: Colors.white.withOpacity(0.5),
                            )
                          ],
                        ),
                        Text(
                          "UNIDADES ---------------------".toString(),
                          maxLines: 1,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey.shade600),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 5),
                            child: Text(
                              "calificacion",
                              textAlign: TextAlign.left,
                              style: TextStyle(color: Colors.yellow),
                            ))
                      ],
                    ));
              }, childCount: 20),
            ),
          ],
        ),
      ),
    );
  }
}

class HeaderWidget extends StatelessWidget {
  final String text;

  HeaderWidget(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Text(text),
      color: Colors.grey[200],
    );
  }
}

class BodyWidget extends StatelessWidget {
  final Color color;

  BodyWidget(this.color);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100.0,
      color: color,
      alignment: Alignment.center,
    );
  }
}

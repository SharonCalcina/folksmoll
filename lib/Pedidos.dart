import 'package:fancy_dialog/FancyAnimation.dart';
import 'package:fancy_dialog/FancyTheme.dart';
import 'package:fancy_dialog/fancy_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/CrearProd.dart';
import 'package:flutter_application_1/ListaCar.dart';
import 'package:flutter_application_1/Products.dart';
import 'package:flutter_application_1/Register.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class Pedidos extends StatefulWidget {
  @override
  _PedidosState createState() => _PedidosState();
}

class _PedidosState extends State<Pedidos> {
  List items = [
    "assets/images/b.jpg",
    "assets/images/b.jpg",
    "assets/images/hp.jpg",
    "assets/images/b.jpg",
    "assets/images/b.jpg",
    "assets/images/b.jpg",
    "assets/images/hp.jpg",
    "assets/images/b.jpg"
  ];
  int currentIndex = 2;
  int valorTotal = 0;

  @override
  Widget build(BuildContext context) {
    int total = 0;
    final listaCar = Provider.of<ListaCar>(context, listen: true);
    for (var i = 0; i < listaCar.listaProd.length; i++) {
      total = total +
          (int.parse(listaCar.listaProd[i].cantidadProductos) *
              int.parse(listaCar.listaProd[i].precio));
    }
    valorTotal = total;
    return Consumer<ListaCar>(
        builder: (context, listaCar, child) => Scaffold(
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                automaticallyImplyLeading: false,
                //leadingWidth: 80,
                toolbarHeight: 60,
                elevation: 15,
                shadowColor: Colors.white,
                excludeHeaderSemantics: false,
                shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.only(bottomRight: Radius.circular(100))),
                centerTitle: true,
                backgroundColor: Colors.pink,
                title: Text(
                  "Carrito de Compras",
                  style: TextStyle(fontSize: 16),
                ),
              ),
              body: listaCar.listaProd.length > 0
                  ? Container(
                      color: Colors.black.withOpacity(0.5),
                      margin: EdgeInsets.only(left: 10, right: 10),
                      child: CustomScrollView(slivers: <Widget>[
                        SliverGrid(
                            delegate:
                                SliverChildBuilderDelegate((context, index) {
                              return Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                color: Colors.white.withOpacity(0.3),
                                child: Row(
                                  children: [
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 10, top: 10, bottom: 10),
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            child: Image.network(
                                              '${listaCar.listaProd[index].imagen}' +
                                                  '?alt=media',
                                              width: 120,
                                              height: 120,
                                              fit: BoxFit.cover,
                                            ))),
                                    Column(
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              width: 180,
                                              //color: Colors.amber,
                                              height: 35,
                                              margin: EdgeInsets.only(
                                                  top: 20, bottom: 5),
                                              padding: EdgeInsets.only(
                                                  left: 5, right: 5),
                                              child: Text(
                                                listaCar.listaProd[index]
                                                    .nombreCompania
                                                    .toUpperCase(),
                                                maxLines: 2,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 14,
                                                    color: Colors.white
                                                        .withOpacity(0.75)),
                                              ),
                                            ),
                                            GestureDetector(
                                              child: IconButton(
                                                  icon: Icon(Icons.close,
                                                      size: 20,
                                                      color: Colors.black
                                                          .withOpacity(0.6)),
                                                  onPressed: () {
                                                    setState(() {
                                                      listaCar.listaProd.remove(
                                                          listaCar.listaProd[
                                                              index]);
                                                    });
                                                  }),
                                            )
                                          ],
                                        ),
                                        Container(
                                            width: 230,
                                            padding: EdgeInsets.only(
                                                left: 5, right: 5, bottom: 5),
                                            child: Text(
                                                listaCar.listaProd[index]
                                                    .descripcion
                                                    .toLowerCase(),
                                                maxLines: 3,
                                                style: TextStyle(
                                                    color: Colors.white
                                                        .withOpacity(0.4),
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    fontSize: 14))),
                                        Container(
                                            //color: Colors.pink,
                                            width: 230,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: <Widget>[
                                                Container(
                                                    // color: Colors.blue,
                                                    width: 85,

                                                    //margin: EdgeInsets.only(left: 2, right: 2, ),
                                                    child: Text(
                                                      "${listaCar.listaProd[index].precio}  Bs.",
                                                      maxLines: 2,
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                          color: Colors.black
                                                              .withOpacity(0.8),
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 16),
                                                    )),
                                                Container(
                                                    //color: Colors.amber,
                                                    padding: EdgeInsets.all(5),
                                                    width: 130,
                                                    // margin: EdgeInsets.only(
                                                    //   left: 5, right: 5, bottom: 5),
                                                    child: Row(
                                                      children: [
                                                        Container(
                                                          width: 30,
                                                          height: 30,
                                                          decoration:
                                                              BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5),
                                                            color: Colors.white
                                                                .withOpacity(
                                                                    0.8),
                                                          ),
                                                          child: IconButton(
                                                              color:
                                                                  Colors.pink,
                                                              icon: Icon(
                                                                Icons.remove,
                                                                size: 15,
                                                              ),
                                                              onPressed: () {
                                                                setState(() {
                                                                  if (int.parse(listaCar
                                                                          .listaProd[
                                                                              index]
                                                                          .cantidadProductos) >=
                                                                      1) {
                                                                    listaCar
                                                                        .listaProd[
                                                                            index]
                                                                        .setcantidad((int.parse(listaCar.listaProd[index].cantidadProductos) -
                                                                                1)
                                                                            .toString());
                                                                  }
                                                                });
                                                              }),
                                                        ),
                                                        Container(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  top: 5),
                                                          decoration:
                                                              BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5),
                                                            color: Colors.black
                                                                .withOpacity(
                                                                    0.5),
                                                          ),
                                                          height: 30,
                                                          width: 60,
                                                          child: Text(
                                                            listaCar
                                                                .listaProd[
                                                                    index]
                                                                .cantidadProductos,
                                                            /* user.productosPedido[
                                                                user.listaCar[
                                                                    index]]
                                                            .toString(),*/
                                                            textAlign: TextAlign
                                                                .center,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        ),
                                                        Container(
                                                            width: 30,
                                                            height: 30,
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              color: Colors.cyan
                                                                  .shade200,
                                                            ),
                                                            child: IconButton(
                                                                icon: Icon(
                                                                  Icons.add,
                                                                  size: 15,
                                                                ),
                                                                onPressed: () {
                                                                  setState(() {
                                                                    if (int.parse(listaCar
                                                                            .listaProd[index]
                                                                            .cantidadProductos) >=
                                                                        0) {
                                                                      listaCar
                                                                          .listaProd[
                                                                              index]
                                                                          .setcantidad(
                                                                              (int.parse(listaCar.listaProd[index].cantidadProductos) + 1).toString());
                                                                    }
                                                                  });
                                                                }))
                                                      ],
                                                    ))
                                              ],
                                            ))
                                      ],
                                    )
                                  ],
                                ),
                              );
                            }, childCount: listaCar.listaProd.length),
                            gridDelegate:
                                SliverGridDelegateWithMaxCrossAxisExtent(
                                    maxCrossAxisExtent: 400,
                                    mainAxisSpacing: 4,
                                    crossAxisSpacing: 10,
                                    childAspectRatio: 2)),
                        SliverList(
                            delegate: SliverChildListDelegate([
                          SizedBox(
                            height: 100,
                          )
                        ]))
                      ]))
                  : SingleChildScrollView(
                      child: Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 100, bottom: 100),
                      child: Column(
                        children: [
                          Text(
                            "Aun no ha añadido productos a su carrito",
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 50),
                            child: Image.asset("assets/images/car.png"),
                          )
                        ],
                      ),
                    )),
              bottomSheet: Container(
                  height: 80,
                  color: Colors.pink.withOpacity(0.8),
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 3,
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 2, right: 2),
                        width: MediaQuery.of(context).size.width / 3,
                        child: Text("TOTAL Bs. " + total.toString(),
                            style: TextStyle(
                                //fontWeight: FontWeight.bold,
                                color: Colors.white,
                                fontSize: 13)),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        child: RaisedButton(
                          elevation: 20,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          onPressed: () => {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) => FancyDialog(
                                      title: "Aceptar compra",
                                      descreption: "Enviar por WhatsApp",
                                      animationType: FancyAnimation.BOTTOM_TOP,
                                      theme: FancyTheme.FANCY,
                                      //gifPath: './assets/images/buy2.gif',
                                      okFun: () => {
                                        msgListaPedido(listaCar),
                                        Navigator.pop(context),

                                        // Navigator.pushNamed(context, HomePage.routeHome),
                                        listaCar.listaProd.clear(),
                                      },
                                    )),
                            //Navigator.pop(context),
                          },
                          child: Text("Confirmar"),
                          color: Colors.cyan.shade200,
                        ),
                      )
                    ],
                  )),
              bottomNavigationBar: BottomNavigationBar(
                onTap: (val) {
                  setState(() {
                    currentIndex = val;
                  });
                  if (currentIndex == 0) {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) {
                      return Products();
                    }));
                  }
                  if (currentIndex == 1) {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) {
                      return CrearProd();
                    }));
                  }
                  if (currentIndex == 3) {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) {
                      return Register();
                    }));
                  }
                },
                currentIndex: currentIndex,
                iconSize: 30,
                showUnselectedLabels: true,
                selectedIconTheme: IconThemeData(color: Colors.cyan.shade200),
                unselectedIconTheme:
                    IconThemeData(color: Colors.white.withOpacity(0.8)),
                backgroundColor: Colors.black,
                unselectedItemColor: Colors.white.withOpacity(0.8),
                selectedItemColor: Colors.cyan.shade200,
                items: [
                  BottomNavigationBarItem(
                      backgroundColor: Colors.black,
                      icon: Image.asset(
                        "assets/images/ICO.png",
                        width: 35,
                        height: 30,
                      ),
                      label: "Inicio"),
                  BottomNavigationBarItem(
                      backgroundColor: Colors.black,
                      icon: Icon(
                        Icons.mail_rounded,
                      ),
                      label: "Mensajes"),
                  BottomNavigationBarItem(
                      backgroundColor: Colors.black,
                      icon: Icon(
                        Icons.shopping_cart,
                      ),
                      label: "Carrito"),
                  BottomNavigationBarItem(
                      backgroundColor: Colors.black,
                      icon: Icon(
                        Icons.account_circle,
                      ),
                      label: "Mi Cuenta"),
                ],
              ),
            ));
  }

  void msgListaPedido(ListaCar lista) async {
    String pedido = "";
    String fecha = DateTime.now().toString();
    pedido = pedido + "FECHA:" + fecha.toString();
    pedido = pedido + "\n";
    pedido = pedido + "MEGA DESCUENTOS A DOMICILIO";
    pedido = pedido + "\n";
    pedido = pedido + "CLIENTE: FOLKS-MOLL";
    pedido = pedido + "\n";
    pedido = pedido + "_____________";

    for (int i = 0; i < lista.listaProd.length; i++) {
      pedido = '$pedido' +
          "\n" +
          "Producto : " +
          lista.listaProd[i].descripcion +
          "\n" +
          "Cantidad: " +
          lista.listaProd[i].cantidadProductos +
          "\n" +
          "Precio : " +
          lista.listaProd[i].precio.toString() +
          "\n" +
          "\_________________________\n";
    }
    pedido = pedido + "TOTAL:" + valorTotal.toString();

    await launch("https://wa.me/${59178799034}?text=$pedido");
  }
}

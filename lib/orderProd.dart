import 'package:flutter_application_1/prodModel.dart';

class OrderProd {
  int cantidad;
  ProdModel producto;
  OrderProd(int cantidad, ProdModel producto);
  OrderProd.map(dynamic obj) {
    this.cantidad = obj['cantidad'];
    this.producto = obj['producto'];
  }
  add(int cant, ProdModel prod) {
    this.cantidad = cant;
    this.producto = prod;
  }

  Map<dynamic, dynamic> toMap() {
    var map = new Map<dynamic, dynamic>();

    map['cantidad'] = cantidad;
    map['producto'] = producto;

    return map;
  }

  OrderProd.fromMap(Map<dynamic, dynamic> map) {
    this.cantidad = map['cantidad'];
    this.producto = map['producto'];
  }
}

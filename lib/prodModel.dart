class ProdModel {
  String id;
  String imagen;
  String descripcion;
  String precio;
  String modelo;
  String marca;
  String origen;
  String otrosDetalles;
  String tiempoDelivery;
  String detallesEnvio;
  String dirEmpresa;
  String nombreContacto;
  String nombreCompania;
  String nroCel;
  String tipoNegocio;
  String prodPrincipal;
  String terminoPago;
  String capacidadProduccion;
  String cantidadProductos;

  ProdModel(
    String id,
    String imagen,
    String descripcion,
    String precio,
    String modelo,
    String marca,
    String origen,
    String otrosDetalles,
    String tiempoDelivery,
    String detallesEnvio,
    String dirEmpresa,
    String nombreContacto,
    String nombreCompania,
    String nroCel,
    String tipoNegocio,
    String prodPrincipal,
    String terminoPago,
    String capacidadProduccion,
    String cantidadProductos,
  );

  ProdModel.map(dynamic obj) {
    this.id = obj['id'];
    this.imagen = obj['imagen'];
    this.descripcion = obj['descripcion'];
    this.precio = obj['precio'];
    this.modelo = obj['modelo'];
    this.marca = obj['marca'];
    this.origen = obj['origen'];
    this.otrosDetalles = obj['otrosDetalles'];
    this.tiempoDelivery = obj['tiempoDelivery'];
    this.detallesEnvio = obj['detallesEnvio'];
    this.dirEmpresa = obj['dirEmpresa'];
    this.nombreContacto = obj['nombreContacto'];
    this.nombreCompania = obj['nombreCompañia'];
    this.nroCel = obj['nroCel'];
    this.prodPrincipal = obj['prodPrincipal'];
    this.terminoPago = obj['terminoPago'];
    this.capacidadProduccion = obj['capacidadProduccion'];
    this.cantidadProductos = obj['cantidadProductos'];
    this.tipoNegocio = obj['tipoNegocio'];
  }
  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();

    map['id'] = id;
    map['imagen'] = imagen;
    map['descripcion'] = descripcion;
    map['precio'] = precio;
    map['modelo'] = modelo;
    map['marca'] = marca;
    map['origen'] = origen;
    map['otrosDetalles'] = otrosDetalles;
    map['tiempoDelivery'] = tiempoDelivery;
    map['detallesEnvio'] = detallesEnvio;
    map['dirEmpresa'] = dirEmpresa;
    map['nombreContacto'] = nombreContacto;
    map['nombreCompañia'] = nombreCompania;
    map['nroCel'] = nroCel;
    map['prodPrincipal'] = prodPrincipal;
    map['terminoPago'] = terminoPago;
    map['capacidadProduccion'] = capacidadProduccion;
    map['cantidadProductos'] = cantidadProductos;
    map['tipoNegocio'] = tipoNegocio;

    return map;
  }

  ProdModel.fromMap(Map<String, dynamic> map) {
    this.id = map['id'];
    this.imagen = map['imagen'];
    this.descripcion = map['descripcion'];
    this.precio = map['precio'];
    this.modelo = map['modelo'];
    this.marca = map['marca'];
    this.origen = map['origen'];
    this.otrosDetalles = map['otrosDetalles'];
    this.tiempoDelivery = map['tiempoDelivery'];
    this.detallesEnvio = map['detallesEnvio'];
    this.dirEmpresa = map['dirEmpresa'];
    this.nombreContacto = map['nombreContacto'];
    this.nombreCompania = map['nombreCompañia'];
    this.nroCel = map['nroCel'];
    this.prodPrincipal = map['prodPrincipal'];
    this.terminoPago = map['terminoPago'];
    this.capacidadProduccion = map['capacidadProduccion'];
    this.cantidadProductos = map['cantidadProductos'];
    this.tipoNegocio = map['tipoNegocio'];
  }

  void setcantidad(String s) {
    this.cantidadProductos = s;
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter_application_1/prodModel.dart';

class ListaCar extends ChangeNotifier {
  List<ProdModel> listaProd = [];
  addProd(prod) {
    listaProd.add(prod);
    notifyListeners();
  }

  removeProd(prod) {
    listaProd.remove(prod);
    notifyListeners();
  }

  int get listProd {
    return listaProd.length;
  }
}

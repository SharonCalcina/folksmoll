class Usuarios {
  var id;
  String pais;
  String correo;
  String nombres;
  String apellidos;
  String empresa;
  Usuarios(
    String docId,
    String pais,
    String correo,
    String nombres,
    String apellidos,
    String empresa,
  );
  Usuarios.map(dynamic obj) {
    this.id = obj['id'];
    this.pais = obj['pais'];
    this.correo = obj['correo'];
    this.nombres = obj['nombres'];
    this.apellidos = obj['apellidos'];
    this.empresa = obj['empresa'];
  }
  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['id'] = id;
    map['pais'] = pais;
    map['correo'] = correo;
    map['nombres'] = nombres;
    map['apellidos'] = apellidos;
    map['empresa'] = empresa;

    return map;
  }

  Usuarios.fromMap(Map<String, dynamic> map) {
    this.id = map['id'];
    this.pais = map['pais'];
    this.correo = map['correo'];
    this.nombres = map['nombres'];
    this.apellidos = map['apellidos'];
    this.empresa = map['empresa'];
  }
}

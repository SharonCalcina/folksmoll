import 'package:flutter_application_1/orderProd.dart';

class PedidoUserModel {
  String id;
  String usuario;
  List<OrderProd> pedido;

  PedidoUserModel(String id, String usuario, List<OrderProd> pedido);
  PedidoUserModel.map(dynamic obj) {
    this.id = obj['id'];
    this.usuario = obj['usuario'];
    this.pedido = obj['pedido'];
  }
  Map<dynamic, dynamic> toMap() {
    var map = new Map<dynamic, dynamic>();

    map['id'] = id;
    map['usuario'] = usuario;
    map['pedido'] = pedido;

    return map;
  }

  PedidoUserModel.fromMap(Map<dynamic, dynamic> map) {
    this.id = map['id'];
    this.usuario = map['usuario'];
    this.pedido = map['pedido'];
  }
}

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/ListaCar.dart';
import 'package:flutter_application_1/ListaProd.dart';
//import 'package:flutter_application_1/Pedidos.dart';
import 'package:flutter_application_1/Products.dart';
import 'package:flutter_application_1/firebase/estados.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //final user = Provider.of<UserRepository>(context, listen: true);
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
              create: (context) => UserRepository.instance()),
          Provider(create: (context) => ListaCar())
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Online App Demo',
          home: MyHomePage(),
          theme: ThemeData(
            primaryColor: Colors.red,
            bottomSheetTheme: BottomSheetThemeData(
                backgroundColor: Colors.black.withOpacity(0)),
          ),
        ));
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    //double h = MediaQuery.of(context).size.height;
    return Products();
  }

  /* Widget tipo(String a, p1, p2, p3, p4) {
    return InkWell(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return Products(p1, p2, p3, p4);
          }));
        },
        highlightColor: Colors.black,
        child: Container(
          color: Colors.cyan.shade200,
          padding: EdgeInsets.all(10),
          child: Align(child: Text(a), alignment: Alignment.centerLeft),
        ));
  }

  Widget tipoA(String a, String p1) {
    return InkWell(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return Products2(p1);
          }));
        },
        highlightColor: Colors.black,
        child: Container(
          color: Colors.cyan.shade200,
          padding: EdgeInsets.all(10),
          child: Align(child: Text(a), alignment: Alignment.centerLeft),
        ));
  }

  Widget tipo2(String a, String p1, String p2, String p3, String p4) {
    return InkWell(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return Products(p1, p2, p3, p4);
          }));
        },
        highlightColor: Colors.pink,
        child: Container(
          color: Colors.black.withOpacity(0.02),
          padding: EdgeInsets.all(10),
          child: Align(child: Text(a), alignment: Alignment.centerLeft),
        ));
  }

  Widget tipo2A(String a, String p1) {
    return InkWell(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return Products2(p1);
          }));
        },
        highlightColor: Colors.pink,
        child: Container(
          color: Colors.black.withOpacity(0.02),
          padding: EdgeInsets.all(10),
          child: Align(child: Text(a), alignment: Alignment.centerLeft),
        ));
  }*/
}

/*drawer: Drawer(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 80,
            ),
            Center(child: Text("Hola")),
            SizedBox(
              height: 20,
            ),
            tipo("Artesanias", "Instrumentos","Juguetes y accesorios", "Ropa y Zapatos", "Otros"),
            tipo2("Ropa, Zapatos, Joyeria, Accesorios", "Ropa", "Zapatos",
                "Joyeria", "Accesorios"),
            tipoA("Electronicos", "Electronicos"),
            tipo2("Hogar y cocina", "utensilios", "despensa", "", "otros"),
            tipoA("Articulos de belleza", "Articulos de belleza"),
            tipo2A("Articulos deportivos", "Articulos deportivos"),
            tipoA("Articulos para mascotas", "Articulos para mascotas"),
            tipo2A("Articulos para bebe", "Articulos para bebe"),
            tipoA("Muebles", "Muebles en general"),
            tipo2A("Juguetes", "Juguetes"),
            tipoA("Construccion", "Construccion"),
            tipo2("Otros", "", "", "", ""),
          ],
        ),
      ),*/

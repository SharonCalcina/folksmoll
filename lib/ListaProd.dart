import 'package:flutter/cupertino.dart';
import 'package:flutter_application_1/orderProd.dart';

class ListaProd extends ChangeNotifier {
  List<OrderProd> listaProd = [];
  addProd( OrderProd prod) {
    listaProd.add(prod);
    notifyListeners();
  }

  removeProd(OrderProd prod) {
    listaProd.remove(prod);
    notifyListeners();
    
  }

  int get listProd {
    return listaProd.length;
  }
}

import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_application_1/ListaCar.dart';
import 'package:flutter_application_1/Register.dart';
import 'package:flutter_application_1/firebase/estados.dart';
import 'package:flutter_application_1/firebase/firebaseArtesania.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/prodModel.dart';
import 'package:provider/provider.dart';

class Artesanias extends StatefulWidget {
  @override
  _ArtesaniasState createState() => _ArtesaniasState();
}

class _ArtesaniasState extends State<Artesanias> {
  List items = [
    "assets/images/b.jpg",
    "assets/images/b.jpg",
    "assets/images/hp.jpg",
    "assets/images/b.jpg",
    "assets/images/b.jpg",
    "assets/images/b.jpg",
    "assets/images/hp.jpg",
    "assets/images/b.jpg"
  ];
  List<ProdModel> listaProd = List<ProdModel>();
  FirebaseArtesania db = new FirebaseArtesania();
  StreamSubscription<QuerySnapshot> productSub;
  @override
  void initState() {
    super.initState();
    listaProd = new List();
    productSub?.cancel();
    productSub = db.getListProd().listen((QuerySnapshot snapshot) {
      final List<ProdModel> products = snapshot.docs
          .map((documentSnapshot) => ProdModel.fromMap(documentSnapshot.data()))
          .toList();
      setState(() {
        listaProd = products;
      });
    });
  }

  @override
  void dispose() {
    productSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserRepository>(context, listen: true);
    //final listaCar = Provider.of<ListaCar>(context, listen: true);
    return Container(
      color: Colors.black.withOpacity(0.9),
      child: CustomScrollView(
        slivers: <Widget>[
          SliverToBoxAdapter(
              child: Container(
                  margin: EdgeInsets.all(15),
                  height: 150,
                  child: Carousel(
                    indicatorBgPadding: 5,
                    boxFit: BoxFit.cover,
                    animationDuration: Duration(microseconds: 5),
                    images: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Image.asset(
                          items[0],
                          fit: BoxFit.cover,
                        ),
                      ),
                      ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: Image.asset(
                            items[6],
                            fit: BoxFit.cover,
                          ))
                    ],
                  ))),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              mainAxisSpacing: 2,
              crossAxisSpacing: 3,
              maxCrossAxisExtent: 100,
              childAspectRatio: 0.9,
            ),
            delegate: SliverChildBuilderDelegate((context, index) {
              int len = 8;
              return index < len - 1
                  ? Container(
                      //elevation: 0,
                      margin: EdgeInsets.all(10),
                      child: Column(
                        children: <Widget>[
                          CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 28,
                              backgroundImage: AssetImage(
                                "assets/images/hp.jpg",
                              )),
                          Container(
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                "listaProd[0].id",
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.pink.shade500,
                                  fontSize: 10,
                                ),
                              )),
                        ],
                      ),
                    )
                  : Container(
                      margin: EdgeInsets.all(10),
                      child: Column(
                        children: <Widget>[
                          CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 29,
                              backgroundImage: AssetImage(
                                "assets/images/menuIcon.png",
                              )),
                          Container(
                            margin: EdgeInsets.only(top: 5),
                            child: Text(
                              "Todos",
                              maxLines: 2,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.pink.shade500,
                                fontSize: 12,
                              ),
                            ),
                          )
                        ],
                      ),
                    );
            }, childCount: 8),
          ),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                mainAxisSpacing: 5,
                crossAxisSpacing: 5,
                maxCrossAxisExtent: 200,
                childAspectRatio: 0.63),
            delegate: SliverChildBuilderDelegate((context, index) {
              return Card(
                  elevation: 1,
                  color: Colors.black,
                  //color: Colors.grey.shade300,
                  child: Column(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          //image:DecorationImage(image: ),
                          borderRadius: BorderRadius.all(Radius.circular(0.8)),
                          //color: Colors.white
                        ),
                        margin: EdgeInsets.all(10),
                        height: 150,
                        width: 200,
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: CachedNetworkImage(
                                imageUrl:
                                    '${listaProd[index].imagen}' + '?alt=media',
                                fit: BoxFit.cover,
                                placeholder: (_, __) {
                                  return Center(
                                      child: CupertinoActivityIndicator(
                                    radius: 15,
                                  ));
                                })),
                      ),
                      Container(
                        padding: EdgeInsets.all(5),
                        height: 58,
                        child: Column(
                          children: <Widget>[
                            Text(
                              '${listaProd[index].descripcion}',
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.white.withOpacity(0.6)),
                              maxLines: 3,
                            ),
                          ],
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.only(left: 5, bottom: 3),
                            child: Text(
                              '${listaProd[index].precio}' + ' BS.',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white.withOpacity(0.9)),
                            ),
                          ),
                          Consumer<ListaCar>(
                              builder: (context, lCar, child) =>
                                  GestureDetector(
                                    //borderRadius: BorderRadius.circular(20),
                                    onTap: () {
                                      if (Status.Authenticated == user.status) {
                                        setState(() {
                                          if (!lCar.listaProd
                                              .contains(listaProd[index])) {
                                            listaProd[index].setcantidad("1");
                                            lCar.listaProd
                                                .add(listaProd[index]);
                                            print(
                                                "objectppppppppppppppppp******************");
                                            print(lCar.listProd.toString() +
                                                "///////////////");
                                            for (var i = 0;
                                                i < lCar.listaProd.length;
                                                i++) {
                                              print(lCar.listaProd[i]
                                                  .toMap()
                                                  .toString());
                                            }
                                          }
                                        });

                                        /*user.productosPedido[listaProd[index]] =
                                          1;*/

                                      } else {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(builder:
                                                (BuildContext context) {
                                          return Register();
                                        }));
                                      }
                                    },
                                    child: Container(
                                      //color: Colors.red,
                                      width: 20,
                                      alignment: Alignment.centerRight,
                                      margin: EdgeInsets.only(
                                          right: 8, bottom: 3, left: 8, top: 5),
                                      child: Icon(
                                        Icons.add_shopping_cart,
                                        color: Colors.cyan.shade200,
                                      ),
                                    ),
                                    //splashColor: Colors.white.withOpacity(0.5),
                                  ))
                        ],
                      ),
                      Text(
                        '${listaProd[index].otrosDetalles.toString()}' +
                            '  Unidades',
                        maxLines: 1,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey.shade600),
                      ),
                      /*Container(
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                "calificacion",
                                textAlign: TextAlign.left,
                                style: TextStyle(color: Colors.yellow),
                              ))*/
                    ],
                  ));
            }, childCount: listaProd.length),
          ),
        ],
      ),
    );
  }
}

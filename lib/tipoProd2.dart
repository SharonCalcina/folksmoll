import 'package:flutter/material.dart';

class Products2 extends StatefulWidget {
  final String _p1;
  Products2(this._p1);
  @override
  _Products2State createState() => _Products2State(this._p1);
}

class _Products2State extends State<Products2> {
  _Products2State(this._p1);
  String _p1;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 1,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.pink,
            flexibleSpace: Column(
              children:<Widget>[
                SizedBox(height: 50,),
                Image.asset("assets/images/LG.png", height: MediaQuery.of(context).size.height / 15,
                      width: 200,),
              ]),
              
            bottom: TabBar(
              indicatorColor: Colors.cyan,
              labelColor: Colors.white,
              indicatorSize: TabBarIndicatorSize.label,
              tabs: <Widget>[
                Tab(
                  child: Text(_p1),
                ),
              ],
            ),
          ),
        ));
  }
}
